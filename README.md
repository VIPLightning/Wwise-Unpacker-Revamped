# 📦Wwise-Unpacker-Revamped
<p align="center" dir="auto">
<img src="https://games.dolby.com/contentassets/3d0f999312ff4ab2b43409e1c2a96b3d/wwisebox2.png">
<p/>
This github project is a fork of [Vextil's Wwise-Unpacker](https://github.com/Vextil/Wwise-Unpacker) and an inspiration/cherrypicked of other people's work that have also forked his project.

What makes this project stand out? The idea was to add support for "FLAC" and convert your game soundtracks, voices, and, sound effects to a lossless and open-source file extension without any sacrifices to your storage space unlike WAV and its proprietary nature. And as for MP3 and OGG, despite its compression, the format eventually loses information making these two not viable in the long run when encoding. That's where FLAC comes in allowing the end user to encode the same file and still retains great sound quality. I have not noticed the sound quality diminishing after testing the differences when playbacking songs at 24 bits per sample @ 44khz (level 5 default) vs. 16 bits per sample at 48khz (level 8). FLAC fills in many of the gaps that WAV, MP3, and OGG cannot do. 


# 📷Screenshots


<img src="https://upload-os-bbs.hoyolab.com/upload/2023/11/12/358965590/18a5e8c4ad4c20d309104dcf90f69328_7487922294818749827.png" width="400" Height="275">
<img src="https://upload-os-bbs.hoyolab.com/upload/2023/11/12/358965590/26a28656e435125a23b0554c140a67e0_1212953440236816828.png" width="400" Height="275">
<img src="https://upload-os-bbs.hoyolab.com/upload/2023/11/12/358965590/9e527083da3a48c7008bbaf849b3fa79_5096576020968912684.png" width="400" Height="275">


# 🐧Linux Users Beware
Do Carefully consider that FLAC, vorbis, and ffmpeg are included inside of majority of linux distributions. If your OS does NOT have the following binaries, consider using one of the commands below before running one of the unpacking scripts depending on what distro or Package Manager you are using.

_APK_ (Alpine Linux)
```
sudo apk update
```
```
xargs sudo apk add < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```

_APT_
```
sudo apt update                      
```
```
xargs sudo apt -y install < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```

_DNF_
```
sudo dnf update
```
```
sudo dnf install < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```

_Flatpak_
```
flatpak update
```
```
xargs flatpak install < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```

_Pacman_
```
sudo pacman -Syu
```
```
pacman -S - < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```
_PKG_*
```
pkg update -f
```
```
xargs pkg install < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```

_Yay_
```
yay -Syu
```
```
xargs yay -S - < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```



_Yum_
```
sudo yum update 
```
```
xargs sudo yum -y install < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```

_Zypper_
```
sudo zypper update
```
```
xargs sudo zypper install < ~/Downloads/Wwise-Unpacker-Revamped/Linux/packages.txt
```


📝**TODO List:**
 * Add Linux OS support √
   * Build Linux Shell scripts √
   * Include Linux Tools binaries √
   * Test it in a Unix-like OS environment √ 
     * Tested on: 
       * Debian 12 systemd | Kernel 6.1.0
       * Artix Linux 20230814 openrc | Kernel 6.6.7                                       
  
  *The idea behind supporting Linux, since Genshin Impact and some of the other games are supported through WINE, this eliminates the need to rely on Windows and makes it universally available in order to unpack and convert game sound files. Also I starred .PKG because I'm looking for someone to test this project in a BSD live environment*
 
 * Add batch script to make easier to change FLAC compression level 1-8 √
   * Merge with the unpacking script. √
 * Rework the Unpack to MP3 batch script (Optional) √
 * Add Python support as an option to extract and decode videogame sounds/voices/OSTs (TBD)
 * Rework how quickBMS extracts sounds and music; extracting to WAV using quickBMS will break playback. However extracting to .WEM will play music and allow you to convert .wem files when using foobar2k √

The unpacking was tested on Genshin Impact. <img src="https://img-os-static.hoyolab.com/avatar/avatar40004.png?x-oss-process=image%2Fauto-orient%2C0%2Finterlace%2C1%2Fformat%2Cwebp%2Fquality%2Cq_80" width="100" height="100">✨ 
---
**Special Thanks To:**
* [expl0it3r](https://github.com/eXpl0it3r/bnkextr) for the bnkextr v2.0
* [VGMStream](https://github.com/vgmstream/vgmstream) r1879
* [Vextil/Wwise-Unpacker](https://github.com/Vextil/Wwise-Unpacker) for sharing his original project
* [FLAC](https://xiph.org/flac/) v1.4.3
* [QuickBMS](https://aluigi.altervista.org/quickbms.htm) v0.12.0.0
* [Trust04zh](https://github.com/Trust04zh/Wwise-Unpacker) for the unpack to wav bat
* [hcs64](https://github.com/hcs64/ww2ogg) for the ww2ogg 0.24 source code
* [April](https://github.com/april/revorb) for the revorb source code on non-Windows platforms
* [Opus Codec](https://www.opus-codec.org/) libopus 1.5.2