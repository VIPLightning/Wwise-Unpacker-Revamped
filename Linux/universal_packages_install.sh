#! /bin/sh

release_file=/etc/os-release
logfile=updater.log
errorlog=updater_errors.log

check_exit_status() {
   if [ $? -ne 0 ]
   then
      echo "An error occured, please check the $errorlog file."
   fi
}

if grep -q "Arch" $release_file || grep -q "Artix" $release_file || grep -q "Manjaro" $release_file || grep -q "Garuda" $release_file || grep -q "Endeavor" $release_file || grep -q "Arco" $release_file
then
   # Archbased update and install packages commands listed in text file
   sudo pacman -Syu 1>>$logfile 2>>$errorlog
   check_exit_status

   sudo pacman -S - < packages.txt 1>>$logfile 2>>$errorlog
   check_exit_status
fi

if grep -q "Debian" $release_file || grep -q "Ubuntu" $release_file || grep -q "Pop" $release_file || grep -q "Linux Mint" $release_file || grep -q "PeppermintOS" $release_file
then
   # Ubuntu or debianbased update and install packages commands listed in text file
   sudo apt update 1>>$logfile 2>>$errorlog
   check_exit_status
   
   xargs sudo apt -y install < packages.txt 1>>$logfile 2>>$errorlog
   check_exit_status
fi

if grep -q "openSUSE" $release_file
then
   # openSUSEbased update and install packages commands listed in text file
   sudo zypper update 1>>$logfile 2>>$errorlog
   check_exit_status
  
   xargs sudo zypper install < packages.txt 1>>$logfile 2>>$errorlog
   check_exit_status
fi

if grep -q "FreeBSD" $release_file || grep -q "GhostBSD" $release_file
then
   # FreeBSDbased update and install packages commands listed in text file
   sudo pkg update -f 1>>$logfile 2>>$errorlog
   check_exit_status
   
   xargs pkg install < packages.txt 1>>$logfile 2>>$errorlog
   check_exit_status
fi

if grep -q "CentOS" $release_file || grep -q "RHEL" $release_file
then
    # RHEL or CentOS based update and install packages commands listed in text file
    sudo dnf update 1>>$logfile 2>>$errorlog
    check_exit_status
   
    xargs sudo dnf install < packages.txt 1>>$logfile 2>>$errorlog
    check_exit_status
fi

if grep -q "Void" $release_file
then
   #Void Linux based update and install packages commands listed in text file
   sudo xbps-install -Su 1>>$logfile 2>>$errorlog
   check_exit_status

   xargs sudo xbps-install -S - < packages.txt 1>>$logfile 2>>$errorlog
   check_exit_status
fi
