#! /bin/sh

./Tools/quickbms  "Tools/wavescan.bms" "Game Files" "Tools/Decoding"
for b in Game\ Files/*.bnk; do "./Tools/bnkextr" $b; done
mv Game\ Files/*.wem Tools/Decoding
for c in Tools/Decoding/*.wem; do "./Tools/vgmstream-cli" -o "?f.wem2" $c; done
rm Tools/Decoding/*.wem
for d in Tools/Decoding/*.wem2; do mv "$d" "OPUS"; done
for f in OPUS/*.wem2; do ffmpeg -i $f -c:a libopus -b:a 192k -vbr on ${f}.opus; done

cat tornado.txt

echo -ne "
      -------------------------------------------------------------

      Unpack finished! Files should be in the 'OPUS' folder

      -------------------------------------------------------------


" 

read -p "Should I delete BNKs and PCKs from the Game Files folder? [Y/N]" ans;
case $ans in
  y|Y)
    for e in Game\ Files/*.PCK; do rm $e; done 
    echo "Files deleted, enjoy your unpacked audio! -/u/Vextil ;)";;
     
  n|N)
    echo "BNKs and PCKs kept, enjoy your unpacked audio! -/u/Vextil ;)";;
  *) 
    echo "invalid option";;
esac
